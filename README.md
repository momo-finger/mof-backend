# MoF backend



## Description

[FR] Ici, nous maintenons l’API de notre application, basé sur Laravel. Nous faisons un big up! à notre équipe dévouée.\
[EN] Here we maintain our application API, based on Laravel. We give a big up! to our dedicated team.

## It‘s a IT company

[FR] Nous développons des innovations avec le duo (hard et soft) dans plusieurs domaines d’activité. En Agriculture, en Éducation, gestion de projet, gestion de finance.\
[EN] We develop innovations with the duo (hard and soft) in several fields of activity. In Agriculture, Education, Project Management, Finance Management.

[By Celerite Holding](https://celeriteholding.com)
Email: contact@celeriteholding.com


```
cd existing_repo
git remote add origin https://gitlab.com/momo-finger/mof-backend.git
git branch -M main
git push -uf origin main
```

***

